﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net.Wifi;


using Absence.Droid;
using Absence.Services;

[assembly: Xamarin.Forms.Dependency( typeof(NetworkInfoImplementation) )]
namespace Absence.Droid
{
    public class NetworkInfoImplementation : INetworkInfo
    {

        public NetworkInfoImplementation() { }

        WifiManager wifiManger = (WifiManager)(Application.Context.GetSystemService(Context.WifiService));

        public string GetSSID()
        {
            if (wifiManger != null)
            {
                return wifiManger.ConnectionInfo.SSID;
            }
            else
            {
                return "WifiManger is NULL";
            }
        }

    }
}