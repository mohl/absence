﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Absence.Services
{
    public interface INetworkInfo
    {
        string GetSSID();
    }
}
