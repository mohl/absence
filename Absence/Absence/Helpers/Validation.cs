﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Absence.Helpers
{
    class Validation
    {

        public static bool IsRequired(string field)
        {
            if (field == null)
            {
                return false;
            }

            var str = field as string;
            return !string.IsNullOrWhiteSpace(str);
        }

    }
}
