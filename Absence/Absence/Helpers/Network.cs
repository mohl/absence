﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;

using Xamarin.Forms;

using Absence;
using Absence.Services;

namespace Absence.Helpers
{
    class Network
    {

        private static string[] Networks = new string[] { "Eal-Wireless", "Eal-Guest" };

        public static bool IsValidWifi = false;

        public static bool IsConnected
        {
            get => CrossConnectivity.Current.IsConnected;
        }

        public static void CheckSSID()
        {
            string SSID = DependencyService.Get<INetworkInfo>().GetSSID();
            SSID = SSID.Trim('"');

            //Debug.WriteLine(SSID);

            if ( Networks.Contains(SSID) )
            {
                IsValidWifi = true;
            }
            else 
            {
                IsValidWifi = false;
            }
        }

        public static bool IsValidConnection()
        {
            return CrossConnectivity.Current.IsConnected;
        }

    }
}
