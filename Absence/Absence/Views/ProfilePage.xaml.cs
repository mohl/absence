﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;

using Absence.Helpers;
using Absence.Models;
using Absence.Data;

namespace Absence.Views
{
    public partial class ProfilePage : ContentPage
    {
        string test;
        Statistic Percentage;

        public ProfilePage()
        {
            BindingContext = test;
            InitializeComponent();
            Task.Run(() => this.GetAttendance()).Wait();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        public async void GetAttendance()
        {
            var result = await LessonManager.GetPercentage();

            Percentage = result;
            test = Percentage.statistics + "%";
            ViewPercentage.Text = test;
        }

        private void LogOut_Clicked(object sender, EventArgs e)
        {
            Settings.RemoveToken();
            Settings.RemoveUserId();
            Application.Current.MainPage = new Login();
        }

    }
}
