﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Absence.Data;
using Absence.Models;

namespace Absence.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SchedulePage : ContentPage
    {
        List<Lesson> lessons;

        public SchedulePage()
        {
            BindingContext = lessons;
            InitializeComponent();
            Task.Run(() => this.RetrieveLessons()).Wait();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        public async Task<bool> RetrieveLessons() 
        {
            var test = await LessonManager.GetAllLessons();

            lessons = test;
            SampleView.ItemsSource = lessons;

            return true; 
        } 
    }
}