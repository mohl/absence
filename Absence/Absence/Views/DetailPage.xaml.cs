﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Absence.Data;
using Absence.Models;

namespace Absence.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailPage : ContentPage
    {
        public Lesson Lesson { get; set; }

        public DetailPage(Lesson lesson)
        {
            this.Lesson = lesson;
            InitializeComponent();

            //details = LessonManager.FindByID(ID);
            this.BindingContext = lesson;
        }

        private async void ReportError_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new TicketPage());
        }
    }
}