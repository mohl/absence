﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Absence.Data;
using Absence.Helpers;
using Absence.Models;
using Absence.Services;

namespace Absence.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TodayPage : ContentPage
    {

        List<Lesson> lessons;

        public TodayPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            Task.Run(() => GetLessonsByDate()).Wait();
            //lessons = LessonManager.Fetch();

            //LessonView.ItemsSource = lessons;
            LessonView.ItemTapped += Item_Tapped;
        }

        public async void GetLessonsByDate() 
        {
            Debug.WriteLine("Get all lessons from lessonmanager");
            lessons = await LessonManager.GetLessonsByDate();

            bool empty = !lessons.Any();

            if (empty) 
            {
                NoLessons.IsVisible = true;
            }
            else
            {
                LessonView.ItemsSource = lessons;
            }
        } 

        private async void Item_Tapped(object o, ItemTappedEventArgs e)
        {
            var list = (ListView) o;
            var lesson = (list.SelectedItem as Lesson);

            Debug.WriteLine(lesson._id);

            DateTime now   = DateTime.Now;
            DateTime end   = Convert.ToDateTime(lesson.end);
            DateTime start = Convert.ToDateTime(lesson.start);

            Debug.WriteLine(now);
            Debug.WriteLine(end);

            if (!Network.IsValidWifi)
            {
                await DisplayAlert(
                    "Error",
                    "You're required to use the campus WiFi to register precense",
                    "OK");
                return;
            }

            if (lesson.isTracked == true)
            {
                await DisplayAlert("Error", "You're already tracked for lesson: \n\n" +
                                   lesson.subject, "OK");
                return;
            }
            else if (now > end)
            {
                await DisplayAlert("Error", "Too late to check in for lesson: \n\n" +
                                   lesson.subject, "OK");
                return;

            }
            else if ( now > start && now < end )
            {

                if (await LessonManager.InsertAttendee(lesson._id))
                {
                    var item = lessons.FirstOrDefault(i => i._id == lesson._id);
                    item.isTracked = true;

                    LessonView.ItemsSource = null;
                    LessonView.ItemsSource = lessons;

                    Debug.WriteLine("Just checking in");

                    await DisplayAlert("Success", "You're checked in", "OK");
                    return;
                }
                else
                {
                    await DisplayAlert("Error", "Could not check in", "OK");
                    return;
                }
            }
            else 
            {
                await DisplayAlert("Error", "Not yet", "OK");
                return;
            }
        }

    }
}