﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Absence.Data;
using Absence.Helpers;

namespace Absence.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {

        private bool Valid = false;

        public Login()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {

            string email = Email.Text;
            string password = Password.Text;

            if (Validate(email, password))
            {
                Debug.WriteLine("Is valid");

                if (await AuthManager.Request(email, password))
                {
                    Debug.WriteLine("Logged in");
                    Application.Current.MainPage = new NavigationPage(new MainPage());
                }
                else
                {
                    Debug.WriteLine("Nope");
                    await DisplayAlert("Error", "Unable to login", "ok");
                }

            }

        }

        private bool Validate(string username, string password)
        {

            if (!Validation.IsRequired(username) || !Validation.IsRequired(password))
            {
                LoginError.IsVisible = true;
            }
            else
            {
                Valid = true;
            }
            
            
            return Valid == true ? true : false;
        }
    }
}