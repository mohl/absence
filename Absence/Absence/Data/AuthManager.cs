﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers; 
using System.Diagnostics;
using Flurl;
using Flurl.Http;

using Absence.Models;
using Absence.Helpers;

namespace Absence.Data
{
    class AuthManager
    {
        public static async Task<bool> Request(string email, string password) 
        {
            dynamic url = await "http://188.226.188.240:8888/api/"
                .AppendPathSegment("user")
                .AppendPathSegment("login")
                .PostUrlEncodedAsync(new 
                {
                    mail = email,
                    pass = password
                })
                .ReceiveJson<Auth>();
            
            Auth response = url;

            if (response.auth)
            {
                Settings.Token = response.token;
                Settings.UserId = response.userId;
                return true;
            }
            else
            {
                Settings.RemoveToken();
                Settings.RemoveUserId();
                return false;
            }
        } 
    }
}