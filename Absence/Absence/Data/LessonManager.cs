﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Absence.Models;
using System.IO;
using Flurl;
using Flurl.Http;
using System.Diagnostics;

using Absence.Helpers;

namespace Absence.Data
{
    class LessonManager
    {
        public static async Task<Statistic> GetPercentage()
        {
            var url = await "http://188.226.188.240:8888/api/"
                .AppendPathSegment("lesson")
                .AppendPathSegment("statistics")
                .AppendPathSegment(Settings.UserId)
                .WithHeader("x-access-token", Settings.Token)
                .GetJsonAsync<Statistic>();

            return url;
        }

        public static async Task<List<Lesson>> GetAllLessons()
        {
            var url = await "http://188.226.188.240:8888/api/"
                .AppendPathSegment("lesson")
                .AppendPathSegment("all")
                .AppendPathSegment(Settings.UserId)
                .WithHeader("x-access-token", Settings.Token)
                .GetJsonAsync<List<Lesson>>();

            return url;
        }

        public static async Task<List<Lesson>> GetLessonsByDate()
        {
            var url = await "http://188.226.188.240:8888/api/"
                .AppendPathSegment("lesson")
                .AppendPathSegment("current")
                .AppendPathSegment(Settings.UserId)
                .WithHeader("x-access-token", Settings.Token)
                .GetJsonAsync<List<Lesson>>();

            Debug.WriteLine("Get lessons by date");

            return url;
        }

        public static async Task<bool> InsertAttendee(int id)
        {
            dynamic url = await "http://188.226.188.240:8888/api/"
                .AppendPathSegment("lesson")
                .WithHeader("x-access-token", Settings.Token)
                .PostJsonAsync(new
                {
                    userId = Settings.UserId,
                    lessonId = id
                })
                .ReceiveJson<Lesson>();

            Lesson response = url;
            string[] att = response.attendees;

            if(att.Contains(Settings.UserId))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
