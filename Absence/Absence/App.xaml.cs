﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Xamarin.Forms;

using Absence.Helpers;
using Absence.Views;

namespace Absence
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            if (!Settings.IsTokenSet)
            {
                this.MainPage = new Login();
            }
            else
            {
                this.MainPage = new NavigationPage(new MainPage());
            }
        }

        protected override void OnStart()
        {
            base.OnStart();

            Network.CheckSSID();
            CrossConnectivity.Current.ConnectivityChanged += HandleConnectionChange;
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes

            Network.CheckSSID();
        }

        private void HandleConnectionChange(object sender, ConnectivityChangedEventArgs e)
        {
            Network.CheckSSID();   
        }
    }
}
