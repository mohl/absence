﻿using System;

using Xamarin.Forms;

namespace Absence.Models
{
    public class Auth
    {
        public bool auth { get; set; }
        public string token { get; set; }
        public string userId { get; set; }
    }
}