﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Absence.Models;

namespace Absence.Models
{
    public class LessonList
    {
        public Lesson data { get; set; }
        public bool isTracked { get; set; }
    }
}

