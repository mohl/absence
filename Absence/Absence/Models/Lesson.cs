﻿using System;
namespace Absence.Models
{
    public class Lesson
    {
        public int _id { get; set; }
        public int __v { get; set; }
        public int groupId { get; set; }
        public string subject { get; set; }
        public string teacher { get; set; }
        public string location { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public string[] attendees { get; set; }
        public bool isTracked { get; set; }
    }
}
